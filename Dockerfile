FROM python:3.7.2

WORKDIR /code

ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0

COPY app.py .
COPY requirements.txt .
COPY test.py .
RUN pip install -r requirements.txt
EXPOSE 5000

CMD ["flask", "run"]
